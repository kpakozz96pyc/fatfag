﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace FatFag.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        { }
        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<Set> Sets { get; set; }

        public DbSet<Workout> Workouts { get; set; }
    }
}