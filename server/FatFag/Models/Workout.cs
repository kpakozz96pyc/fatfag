﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;

namespace FatFag.Models
{
    public class Workout
    {
        public DateTime BeginDateTime { get; set; }

        public DateTime? EndDateTime { get; set; }

        public string Description { get; set; }

        public virtual List<Set> Sets { get; set; }

        public int Id { get; set; }
    }
}
