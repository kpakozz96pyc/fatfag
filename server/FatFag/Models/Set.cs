﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;

namespace FatFag.Models
{
    public class Set
    {
        public int Id { get; set; }

        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }

        public int RepsNumber { get; set; }

        public int Weight { get; set; }        

        public DateTime? Duration {get ;set;}
    }
}
