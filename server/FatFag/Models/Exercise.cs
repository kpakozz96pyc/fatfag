﻿using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FatFag.Models
{
    public class Exercise
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public int Id { get; set; }
    }
}
