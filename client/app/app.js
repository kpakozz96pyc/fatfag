"use strict";

(function () {
    angular.module("seed", [
        'ui.router',
        'ui.validate',
        'ngAnimate',
        'focus-if',
        'seed.core'
    ])
        .config(SeedConfig)
        .run(SeedRun);

    function SeedConfig($urlRouterProvider, $locationProvider) {
        //Ui-Router config
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('404');
    }
    function SeedRun() {    }
}());