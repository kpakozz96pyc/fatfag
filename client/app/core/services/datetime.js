"use strict";

(function () {
    angular.module("seed.core")
        .factory("SeedDateTime", SeedDateTime);

    function SeedDateTime() {
        return{
            GetCurrent: getCurrent, // current time zone
            GetFormat: getFormat
        }

        function getCurrent() {
            return moment().unix();
        };

        function getFormat(showTime) {
            return angular.isUndefined(showTime) || showTime ? 'DD MMM YYYY. HH:mm' : 'DD MMM YYYY';
        }
    }
})();