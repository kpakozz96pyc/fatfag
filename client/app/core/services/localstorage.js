"use strict";

(function () {

    angular.module("seed.core")
        .factory("SeedLocalStorage", SeedLocalStorage);

    function SeedLocalStorage() {
        return {
            Get: get,
            Set: set,
            Remove: remove
        }

        function set(name, value) {
            localStorage.setItem(name, value);
            return value;
        }

        function get(name) {
            return localStorage.getItem(name);
        }

        function remove(name) {
            return localStorage.removeItem(name);
        }

    }

})();